//-------------- NASTAVENIA UZIVATELA -------------------------------------------------------------------------------------------------------------------------

const float WaterAmount = 2.5;         //Mnozstvo precerpanej vody v litroch za 24 hodin (POZOR: Hodnota je určená prietokom cerpadla a hodonta sa môže trochu lisit
const int NumberOfCycles = 3;        //Pocet polievacich cyklov za 24 hodin
const float FirstCycle = 7.0;          //Cas, za ktory zacne prvy polievaci cyklus v hodinach

const float WaterReservoir = 45.00;    //Objem rezervoaru v litroch. Vhodnejsie je uviest trochu nizsiu hodnotu
const float PumpFlow = 23.26;       //Prietok vody cerpadlom v mililitroch za sekundu
const int DelayOfPumping = 3000;   //Oneskorenie polievania v milisekundách

//--------------------------------------------------------------------------------------------------------------------------------------------------------------

#include <EEPROM.h>
unsigned long time;
unsigned long delOfProgram;
float hours;
byte i = 0;
unsigned long k = 1;
boolean l = false;
float Interval = 24.00/NumberOfCycles;
byte backUpWateringCycles = 0;
bool backUpNote = false;
bool backUp = false;
bool backUpPump = false;

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(8, INPUT_PULLUP);
  pinMode(10, OUTPUT);
//  readWholeEEPROM(); // uvoľni, ak chceš načítať celú EEPROM pamäť
//  clrEEPROM(); // uvoľni, ak chceš vymazať EEPROM pamäť. Nezabudni uzavrieť initEEPROM() ak nechceš začať prepis.
//  progParam(); // uvoľni, ak chceš vidieť parametre najbližšieho programu
  initEEPROM();
  delOfProgram = millis(); //oneskorenie štartu programu
}

void loop() {
  time = millis() - delOfProgram;
  watering();
  programBackUp();
  ProgramInfo();
}

void watering() {
  if (time >= (FirstCycle + i*Interval)*3600000 && 
      time <  (FirstCycle + i*Interval)*3600000 + 50 && l == false) {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(2, HIGH);
    writeEEPROM();
    Serial.print("Pumping ON     Action Number: ");
    if (i < 9) {Serial.print("0");}
    Serial.print(i+1);
    Serial.print("  Time of action: ");
    Serial.println(float((millis() - delOfProgram)/1000.0),3);
    l = true;
  }
  if (time >= (FirstCycle + i*Interval)*3600000 + (WaterAmount*1000/((PumpFlow/1000)*NumberOfCycles) + DelayOfPumping) && 
      time <  (FirstCycle + i*Interval)*3600000 + (WaterAmount*1000/((PumpFlow/1000)*NumberOfCycles) + DelayOfPumping) + 50 && l == true) {
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(2, LOW);
    writeEEPROM();
    Serial.print("Pumping OFF                       Time of action: ");
    Serial.println(float((millis() - delOfProgram)/1000.0),3);
    i++;
    l = false;
  }
  if (i == int(WaterReservoir/(WaterAmount/NumberOfCycles))) {
    Serial.println("Program terminated. Refill and restart");
    digitalWrite(2, LOW);
    while (i == int(WaterReservoir/(WaterAmount/NumberOfCycles))) {
      blinking();
      }
  }
}

void programBackUp() {
  unsigned long backUpSwitchTime;
  unsigned long backUpTime;
  boolean backUpPin;
  backUpPin = digitalRead(8);
  if (backUpPin == HIGH && backUp == false) {
    backUpSwitchTime = millis() - delOfProgram;
    delay(10);
    if (backUpPin == HIGH) {
      backUp = true;
      backUpTime = millis() - delOfProgram;
      backUpWateringCycles++;
    }
  }
  else if (backUpPin == LOW && backUp == true) {
    backUpSwitchTime = millis() - delOfProgram;
    delay(10);
    if (backUpPin == LOW) {
      backUp = false;
    }
  }
  
  if (time >= backUpTime + (Interval * 3600000) + 10000 && time < backUpTime + (Interval * 3600000) + 10050) {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(2, HIGH);
    backUpPump = true;
    writeEEPROM();
    Serial.print("BackUp Pumping ON           Time Of Action: ");
    Serial.println(float((millis() - delOfProgram)/1000.0),3);
    delay((WaterAmount*1000/((PumpFlow/1000)*NumberOfCycles)+DelayOfPumping));
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(2, LOW);
    writeEEPROM();
    Serial.print("BackUp Pumping OFF          Time Of Action: ");
    Serial.println(float((millis() - delOfProgram)/1000.0),3);
    backUpWateringCycles++;
    backUpTime = millis() - delOfProgram;
    i++;
  }
  
  while (backUpWateringCycles == int(WaterReservoir/(WaterAmount/NumberOfCycles)) && backUpPump == true) {
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(2, LOW);
    Serial.println ("Program has terminated on BackUp");
    blinking();
  }
}

void blinking() {
  static byte dec;
  static byte uni;
   for (int j = 0; j < 9; j++){
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
   }
  digitalWrite(LED_BUILTIN, HIGH);
  delay(2000);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1500);
      dec = i/10;
      uni = i-10*dec;
      for (int j = 1; j <= dec; j++) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(350);
        digitalWrite(LED_BUILTIN, LOW);
        delay(150);
      }
      if (dec > 0) {delay(350);}
      for (int j = 1; j <= uni; j++) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(20);
        digitalWrite(LED_BUILTIN, LOW);
        delay(480);
      }
    delay(1020);
}

void ProgramInfo() {
  static byte dec;
  static byte uni;
  if (time/12000 >= k && time/12000 < k + 0.003){
    if ((time >= (FirstCycle + i*Interval)*3600000 - 12050 && 
         time <  (FirstCycle + i*Interval)*3600000 + 20) || l == true) {
      k++;
    }
    else {
      k++;
      dec = i/10;
      uni = i-10*dec;
      for (int j = 1; j <= dec; j++) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(350);
        digitalWrite(LED_BUILTIN, LOW);
        delay(150);
      }
      if (dec > 0) {delay(350);}
      for (int j = 1; j <= uni; j++) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(20);
        digitalWrite(LED_BUILTIN, LOW);
        delay(480);
      }
    }
  }
}
