void progConditions() { 
  //-------------- PODMIENKY PRED SPUSTENIM PROGRAMU -----------------------------------

  if (WaterAmount >= PumpFlow*86.4) {
    Serial.println("ERROR: You have exceeded maximum capacity of the pump");
  }
  if (WaterReservoir < WaterAmount/NumberOfCycles) {
    Serial.println("ERROR: One cycle requires more water than reservoir capacity");
  }
  if (int(WaterReservoir/(WaterAmount/NumberOfCycles)) > 99) {
    Serial.println("ERROR: Too many cycles in the program");
  }
  if (float(int(WaterReservoir/(WaterAmount/NumberOfCycles))*Interval - Interval + FirstCycle)/24 >= 253.00) {
    Serial.println("ERROR: Too long program. Maximum duration cannot exceed 10 days due to memory restriction");
  }
  while(WaterAmount >= PumpFlow*86.4 || WaterReservoir < WaterAmount/NumberOfCycles || int(WaterReservoir/(WaterAmount/NumberOfCycles)) > 99 || float(int(WaterReservoir/(WaterAmount/NumberOfCycles))*Interval - Interval + FirstCycle)/24 >= 253.00) {
    blinking();  
  }
  //-------------------------------------------------------------------------------------
}

void progParam() {
  //-------------- SUHRNNE INFO O PROGRAME PRED JEHO SPUSTENIM --------------------------   
  Serial.println("----- PARAMETRE NALEDUJÚCEHO PROGRAMU -----");
  Serial.print("Water consumption:               ");
  Serial.print(WaterAmount);
  Serial.println(" l/day");
  Serial.print("Number of watering cycles:       ");
  Serial.print(NumberOfCycles);
  Serial.println(" /day");
  Serial.print("First watering starts in:        ");
  Serial.print(FirstCycle,3);
  Serial.println(" hrs");
  Serial.print("Total number of watering cycles: ");
  Serial.println(int(WaterReservoir/(WaterAmount/NumberOfCycles)));
  Serial.print("Delay between cycles:            ");
  Serial.print(Interval,4);
  Serial.println(" hrs");
  Serial.print("Time of pumping per cycle:       ");
  Serial.print(WaterAmount*1000/((PumpFlow/1000)*NumberOfCycles)+DelayOfPumping);
  Serial.println(" ms");
  Serial.print("Amount of water per cycle:       ");
  Serial.print(WaterAmount*1000/NumberOfCycles);
  Serial.println(" ml");
  Serial.print("Expected end of the program in:  ");
  Serial.print(int(WaterReservoir/(WaterAmount/NumberOfCycles))*Interval - Interval + FirstCycle);
  Serial.println(" hrs");
  Serial.println("----- PARAMETRE NASLEDUJÚCEHO PROGRAMU -----" "\n");
  //-------------------------------------------------------------------------------------
}
