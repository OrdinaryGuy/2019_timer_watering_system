int address = -1;
int startingAddress;
int measurement = 1;
float numOfData = 0.00;
byte value;

void addCycleUp() {
  if (address == EEPROM.length() - 1) {
    address = 0;
  }
  else {
    address++;
  }
}

void addCycleDown() {
  if (address == 0) {
    address = EEPROM.length() - 1;
  }
  else {
    address--;
  }
}

void initDel () {
  byte pulseLength;
  byte standBy;
  float blinkRate;
  delOfProgram = millis();
  while ((millis() - delOfProgram) < 60000) {
    if ((millis() - delOfProgram) < 40000) {
      blinkRate = 0.000006;
    }
    else if ((millis() - delOfProgram) < 53000) {
      blinkRate = 0.00002;
    }
    else {
      blinkRate = 0.00008;
    }
  pulseLength = 125*(-cos(micros()*blinkRate)+1);
  standBy = 250 - pulseLength;
  digitalWrite(LED_BUILTIN, HIGH);
  delayMicroseconds(pulseLength);
  digitalWrite(LED_BUILTIN, LOW);
  delayMicroseconds(10*standBy);
  }
}

void initWrite() {
    EEPROM.write(address, 253);
    addCycleUp();
    EEPROM.write(address, 254);
}

void clrEEPROM() {
  for (int address = 0; address < EEPROM.length(); address++){
    value = EEPROM.read(address);
    if (value != 255) {
      EEPROM.write(address, 255);
    }
  }
}

void initEEPROM() { //Posledná zapísaná hodnota je vždy DEC 254, BIN 1111 1110
  address = -1; //Aby prvá načítaná hodnota bola z addresy EEPROM 0
  int emptyMemory = 0;
  progConditions();
  do {
    addCycleUp();
    value = EEPROM.read(address);
    if (value == 255) {
      emptyMemory++;
    }
  } while (value != 254 && address != EEPROM.length() - 1); //Operácie && a || sú naopak pre tento cyklus
  if (value == 254) {
    readEEPROM();
    Serial.println (F("Hore je uvedená história posledných 5 meraní v chronologickom slede. Pre zobrazenie celej histórie uvoľnite" "\n" 
                    "v zdrojovom kóde funkciu 'readWholeEEPROM()'. Samotný program sa spustí približne za jednu minútu" "\n"));
    progParam();
    initDel();
    do {
      addCycleUp();
      value = EEPROM.read(address);
    } while (value != 254);
    initWrite();
  }
  else if (emptyMemory == EEPROM.length() - 1) {
    Serial.println ("EEPROM pamäť je pripravená na zápis. Program sa spustí približne za minútu." "\n");
    address = 0;
    progParam();
    initDel();
    initWrite();
  }
  else if (address == EEPROM.length() - 1 && value != 254) { 
    address = 0;
    while (address < EEPROM.length()) {
      for (int i = 0; i < 8; i++) {
        value = EEPROM.read(address);
        Serial.print(value);
        Serial.print("\t");
        address++;
      }
      Serial.println("");
    }
    Serial.println (F("\n"  "POZOR! VYBERTE ZARIADENIE Z NAPÁJANIA" "\n" 
                          "Ponechaním zariadenia v napájaní viac ako minútu, prídete o všetky súčasné údaje v EEPROM pamäti," "\n" 
                          "ktoré budú automaticky prepísané na hodnotu 255. Zabrániť tejto procedúde môžete vytiahnutím ""\n"
                          "zariadenia zo zdroja napájania. Prepísanie pamäti je nevyhnutný proces pre správny chod programu.""\n"
                          "Súčasné hodnoty sú vypísané vyššie postupne od počiatočnej bunky v riadkoch po 8 hodnotách." "\n" 
                          "Zariadenie indikuje minútu pred prepisom poblikávaním so stále rastúcou frekvenciou. Po ukončení ""\n"
                          "dochádza ku prepisu a začína progam." "\n"));
    progParam();
    initDel();
    clrEEPROM();
    address = 0;
    initWrite();
  } 
}

void readEEPROM() {
  do {
    findNewMeasurement();
    writeData();
  } while (value != 254 && value != 255 && measurement <= 5);
}

void readWholeEERPOM() {
  do {
    findNewMeasurement();
    writeData();
  } while (value != 254 && value != 255);
}

void findNewMeasurement() {
  numOfData = -1;
  do {
    addCycleDown();
    value = EEPROM.read(address);
    numOfData++;
  } while (value != 253 && value != 254 && value != 255);
  startingAddress = address;
}

void writeData() {
  int zaznam = 1;
  float firstVal = numOfData/5 - int(numOfData/5);
  if (value != 254 && value != 255) {
    Serial.print ("MERANIE ");
    Serial.print (measurement);
    Serial.print ("\t\t" "Počet záznamov: ");
    Serial.println (int(numOfData+4)/5);
    Serial.print("\t\t\t" "den" "\t" "hod" "\t" "min" "\t" "sek" "\t" "sto" "\n");
  }
    do {
      addCycleUp();
      value = EEPROM.read(address);
      if (value == 253 || value == 254 || value == 255) {
        break;
      }
      addCycleDown();
      Serial.print("Záznam č.:  ");
      Serial.print(zaznam);
      Serial.print("\t\t");
      if (firstVal == 0.2 && zaznam == 1) {
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t\t\t\t");
        Serial.print (value);
        goto nextLine;
      }
      else if (firstVal == 0.4 && zaznam == 1) {
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t\t\t");
        Serial.print (value);
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t");
        Serial.print (value);
        goto nextLine;
      }
      else if (firstVal == 0.6 && zaznam == 1) {
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t\t");
        Serial.print (value);
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t");
        Serial.print (value);
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t");
        Serial.print (value);
        goto nextLine;
      }
      else if (firstVal == 0.8 && zaznam == 1) {
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t");
        Serial.print (value);
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t");
        Serial.print (value);
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t");
        Serial.print (value);
        addCycleUp();
        value = EEPROM.read(address);
        Serial.print ("\t");
        Serial.print (value);
        goto nextLine;
      }
      for (int i = 0; i < 5; i++){
        addCycleUp();
        value = EEPROM.read(address);
        if (value == 253 || value == 254 || value == 255) {
          break;
        }
        Serial.print(value);
        Serial.print("\t");
      }
      nextLine:
      zaznam++;
      Serial.println("");
    } while (value != 253 && value != 254 && value != 255);
  Serial.println("");
  address = startingAddress;
  value = EEPROM.read(address);
  measurement++;
}

void writeEEPROM() {
  static unsigned long days, hours, minutes, seconds, hundredths;
  days = time/86400000;
  hours = (time - (days * 86400000))/3600000;
  minutes = (time - (days * 86400000) - (hours * 3600000))/60000;
  seconds = (time - (days * 86400000) - (hours * 3600000) - (minutes * 60000))/1000;
  hundredths = (time - (days * 86400000) - (hours * 3600000) - (minutes * 60000) - (seconds * 1000))/10;
  EEPROM.write(address, days);
  addCycleUp();
  EEPROM.write(address, hours);
  addCycleUp();
  EEPROM.write(address, minutes);
  addCycleUp();
  EEPROM.write(address, seconds);
  addCycleUp();
  EEPROM.write(address, hundredths);
  addCycleUp();
  EEPROM.write(address, 254);
}
